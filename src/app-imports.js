


import vue from 'vue'



/// CSS
import './assets/css/reset.css'

/// Vanilla
import axios from 'axios'

/// Vue
import vueStash from 'vue-stash'
vue.use(vueStash)
