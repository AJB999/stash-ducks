


import Vue from 'vue'
Vue.config.productionTip = false
import app from '@/app.vue'

new Vue(app).$mount('#app')
