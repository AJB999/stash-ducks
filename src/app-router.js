


import vue from 'vue'
import router from 'vue-router'
vue.use(router)



import todos from '@/todos/todos.vue'



export default new router({
	mode : 'history',
	routes : [
		{
			path : '/',
			name : 'todos',
			component : todos
		},
		{
			path : '*',
			component : todos
		}
	]
})
