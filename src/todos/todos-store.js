


import axios from 'axios'



let store = {}

store.data = {
	todos : []
}

store.validate = function(data) {

	/// Will get back to this ...
	return true

	const schema = {
		type : 'object',
		properties : {
			id : { type : 'number' },
			title : { type : 'string' },
			completed : { type : 'boolean' },
			userID : { type : 'number' }
		}
	}

	return validateJSON(schema, data)

}

store.fetchTodos = async function() {
	const todos = await axios.get('https://jsonplaceholder.typicode.com/todos')
	console.log('Done fetching todos')
	this.data.todos = todos.data
}

store.numTodos = function() {
	return this.data.todos.length
}

store.listTodos = function() {
	return this.data.todos
}

store.getTodo = function(todoID, remote = true) {

	/// Maybe check fist if it exists locally,
	/// and if not then fetch it from the API?

	/// Maybe remote === true would force a call
	/// to the API to ensure its not stale data?

	/// For now we'll just return it from the
	/// this.data.todos array
	const todo = this.data.todos.filter((item) => {
		return item.id === todoID
	})

	return todo

}

store.insertTodo = async function(todo) {

	/// Handle validation ...
	if (!this.validate(todo)) {
		return console.error('Invalid todo data.')
	}

	/// Post it to the API layer.
	const url = 'https://jsonplaceholder.typicode.com/todos'
	const result = await axios.post(url)

	/// Then you would push the resulting data
	/// to the local this.data.todos array.

	/// For now we'll just fake it.
	this.data.todos.push(todo)

}

store.updateTodo = async function(todoID, todo) {

	/// Handle validation ...
	if (!this.validate(todo)) {
		return console.error('Invalid todo data.')
	}

	/// First you would post the update to the API layer.
	const url = 'https://jsonplaceholder.typicode.com/todos'
	const result = await axios.post(url)

	/// And then you would update the local this.data.todos
	/// array with the resulting data.

	/// For now we'll just fake it.
	let $this = this
	this.data.todos.map((item, index) => {
		if (item.id === todoID) {
			$this.data.todos[index] = todo
			console.log(`Updated ${todo.id}:`)
			console.log($this.data.todos[index])
		}
	})

}

store.removeTodo = async function(todoID) {

}



export default store
