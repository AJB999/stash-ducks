webpackJsonp([1],{

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(34);

/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _vue = __webpack_require__(2);

var _vue2 = _interopRequireDefault(_vue);

var _vueRouter = __webpack_require__(40);

var _vueRouter2 = _interopRequireDefault(_vueRouter);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_vue2.default.use(_vueRouter2.default);

exports.default = new _vueRouter2.default({
	mode: 'history',
	routes: [{
		path: '/',
		name: 'home'
	}, {
		path: '*'
	}]
});

/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(35)

var Component = __webpack_require__(38)(
  /* script */
  __webpack_require__(32),
  /* template */
  __webpack_require__(39),
  /* scopeId */
  "data-v-3915380a",
  /* cssModules */
  null
)

module.exports = Component.exports


/***/ }),

/***/ 32:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var _axios = __webpack_require__(15);

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $cmp = {
	name: 'app',
	methods: {}
};

$cmp.data = function () {
	return {
		stations: [],
		status: [],
		list: []
	};
};

$cmp.created = async function () {
	var _this = this;

	var stations = await this.fetchStations();
	var status = await this.fetchStatus();

	this.stations = stations.data.data.stations;
	this.status = status.data.data.stations;

	this.list = this.stations.map(function (station) {
		return _extends({}, _this.status.find(function (item) {
			return item.station_id === station.station_id && item;
		}), station);
	});

	this.sortList();
};

$cmp.methods.fetchStations = function () {
	var stationsURL = 'https://toronto-us.publicbikesystem.net/ube/gbfs/v1/en/station_information';
	return _axios2.default.get(stationsURL);
};

$cmp.methods.fetchStatus = function () {
	var statusURL = 'https://toronto-us.publicbikesystem.net/ube/gbfs/v1/en/station_status';
	return _axios2.default.get(statusURL);
};

$cmp.methods.sortList = function ($event) {

	var filter = $event && $event.target.value || 'sort-name';

	if (filter === 'sort-name') {
		this.list = this.list.sort(function (a, b) {
			if (a.name < b.name) {
				return -1;
			}
		});
		return;
	}

	if (filter === 'sort-num') {
		this.list = this.list.sort(function (a, b) {
			if (a.num_bikes_available > b.num_bikes_available) {
				return -1;
			}
		});
		return;
	}

	console.error('You must provide a filter arg for the sortFilter() method.');
	return [];
};

exports.default = $cmp;

/***/ }),

/***/ 33:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _vue = __webpack_require__(2);

var _vue2 = _interopRequireDefault(_vue);

__webpack_require__(12);

var _appRouter = __webpack_require__(13);

var _appRouter2 = _interopRequireDefault(_appRouter);

var _app = __webpack_require__(14);

var _app2 = _interopRequireDefault(_app);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_vue2.default.config.productionTip = false;

new _vue2.default({
	el: '#app',
	router: _appRouter2.default,
	template: '<app/>',
	components: {
		app: _app2.default
	}
});

/***/ }),

/***/ 34:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 35:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 39:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "app"
  }, [_c('div', {
    staticClass: "nav"
  }, [_c('select', {
    on: {
      "change": function($event) {
        return _vm.sortList($event)
      }
    }
  }, [_c('option', {
    attrs: {
      "value": "sort-name"
    }
  }, [_vm._v("Sort by station name")]), _c('option', {
    attrs: {
      "value": "sort-num"
    }
  }, [_vm._v("Sort by bikes available")])])]), _c('div', {
    staticClass: "list"
  }, _vm._l((_vm.list), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "list-item"
    }, [_vm._v(_vm._s(item.name) + " (" + _vm._s(item.num_bikes_available) + " available)")])
  }), 0)])
},staticRenderFns: []}

/***/ })

},[33]);
//# sourceMappingURL=app.7acbd87d1cdadda05afd.js.map